import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { ChatComponent } from './components/chat/chat.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {path: '', redirectTo : 'login', pathMatch : 'full'},
  {path: 'login', component : LoginComponent, pathMatch : 'full'},
  {path : 'chat', component : ChatComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: '**', redirectTo : 'login', pathMatch : 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
