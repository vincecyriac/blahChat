// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCul_87F7Qgroig06oOL9ZJEIpwpwtFvMs",
    authDomain: "proj-blahchat.firebaseapp.com",
    databaseURL: "https://proj-blahchat-default-rtdb.firebaseio.com",
    projectId: "proj-blahchat",
    storageBucket: "proj-blahchat.appspot.com",
    messagingSenderId: "915908430254",
    appId: "1:915908430254:web:c1f95e0f12e11d08b1603f",
    measurementId: "G-KQCVCFZQM1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
