import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  blnSubmitted : boolean = false;

  loginForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, this.noWhitespaceValidator])
  });

  ngOnInit(): void {
    localStorage.removeItem('user')
  }

    //custom whitespace validator
    public noWhitespaceValidator(control: FormControl) {
      const isWhitespace = (control.value || '').trim().length === 0;
      const isValid = !isWhitespace;
      return isValid ? null : { 'whitespace': true };
    }

    login(form:any){
      this.blnSubmitted = true;
      if(this.loginForm.valid){
        localStorage.setItem('user', btoa(form.value.name)) 
        this.router.navigate(['/chat'])
      }
    }
  

}
