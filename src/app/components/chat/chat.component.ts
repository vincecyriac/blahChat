import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgScrollbar } from 'ngx-scrollbar';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  @ViewChild(NgScrollbar) scrollbarRef!: NgScrollbar;

  strCurrentUser!:any;
  arrMessages:any=[];
  dbData:  AngularFireList<any>;

  chatForm: FormGroup = new FormGroup({
    message: new FormControl('')
  });


  constructor(afDb:AngularFireDatabase, private router:Router) {

    this.dbData  = afDb.list('messages');


    this.dbData.valueChanges().subscribe((Response) => {
      console.log(Response);
      
      this.arrMessages = Response
      
    },
      (Error) => {
        console.error("Error");
      });

  }

  ngOnInit(): void {
    let uname:any = localStorage.getItem('user')
    this.strCurrentUser = atob(uname)
  }

  logOut(){
    localStorage.removeItem('user');
    this.router.navigate(['/login'])
  }

  sendData(form:any){
    if(form.value.message){
      this.dbData.push({ user: this.strCurrentUser, message: form.value.message, time: '13:25'});
      this.chatForm.reset()
    }
  }

}
